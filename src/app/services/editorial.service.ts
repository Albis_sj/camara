import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class EditorialService {


  editoriales!: I_editorial[]

  constructor() { }


  agregarNewEditorial(editorial: I_editorial){
    console.log(editorial);

    if(localStorage.getItem('Editoriales') === null){
      this.editoriales = [];
      this.editoriales.unshift(editorial);
      localStorage.setItem('Editoriales', JSON.stringify(this.editoriales))
    } else {
      this.editoriales = JSON.parse(localStorage.getItem('Editoriales') || '');
      this.editoriales.unshift(editorial);
      localStorage.setItem('Editoriales', JSON.stringify(this.editoriales))
    } 
    // console.log(this.listaRegistro, 'lista de agregarTablas');
  }

  ObtenerLocalStorage(){

    if(localStorage.getItem('Editoriales') === null){
      console.log('no comments');
      this.editoriales = []
      return false

    } else {
      console.log('podría dar error');

      let editorialLista= JSON.parse(localStorage.getItem('Editoriales') || '')
      console.log(editorialLista);
      this.editoriales = editorialLista;
      return editorialLista
      
    }
  }

  eliminarEditorial(categoria: I_editorial){

    console.log('llego al SErvidor de Editorial');
    
    const response = confirm('Estás seguro?')

    if(response){
      for(let i = 0; i < this.editoriales.length; i ++){
        if(categoria == this.editoriales[i]){
          // console.log('eli');
          
          this.editoriales.splice(i, 1);
          localStorage.setItem('Editoriales', JSON.stringify(this.editoriales))
        }
      }
      
    }

    
    this.editoriales = this.editoriales.filter(data => {
      return data.nombre!== categoria.nombre
  })

  }

}

export interface I_editorial{
  nombre: string,
    telefono1?: number,
    telefono2?: number
    celular1?: number,
    celular2?: number
  direccion: string,
    correo1?: string,
    correo2?: string
  url?: string
}