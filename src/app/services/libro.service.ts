import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { LibroI } from '../interfaces/libro.interface';

@Injectable({
  providedIn: 'root'
})
export class LibroService {

  listaLibros: LibroI[] = [];

  constructor() {  }

  // snackbar(number : number): void{
  //   if(number = 1){

  //   }
  // }

  agregarLibro(libro: LibroI){
    console.log('service libro');
    console.log(libro);
    
    if(localStorage.getItem('Libros') === null){
      this.listaLibros = [];
      this.listaLibros.unshift(libro);
      localStorage.setItem('Libros', JSON.stringify(this.listaLibros))
    } else {
      this.listaLibros = JSON.parse(localStorage.getItem('Libros') || '');
      this.listaLibros.unshift(libro);
      localStorage.setItem('Libros', JSON.stringify(this.listaLibros))
    }
    // console.log(this.listaLibros, 'lista de agregarTablas');
  }

  agregarlibro(libro: LibroI) {
    this.listaLibros.unshift(libro);
    // console.log(this.listaLibros);
    console.log('Libro I');
    
  }

  buscarlibro(id: string): LibroI{
    return this.listaLibros.find(e => e.titulo=== id) || {} as LibroI
  }

  obtenerLocalStorage(){
    let libroLista = JSON.parse(localStorage.getItem("Libros") || '');
    // console.log(libroLista);
    
    this.listaLibros = libroLista
    // console.log(libroLista, 'obtener LocalStorage');
    return libroLista
  }


  eliminarlibro(libro: LibroI){
  // console.log('intento eliminar');
  // console.log(libro, 'elimiar');
  const response = confirm('Estás seguro que deseas eliminar la libro?')

  if(response){
    for(let i = 0; i < this.listaLibros.length; i ++){
      if(libro == this.listaLibros[i]){
        // console.log('eli');
        
        this.listaLibros.splice(i, 1);
        localStorage.setItem('Libros', JSON.stringify(this.listaLibros))
      }
    }
    
  }

    this.listaLibros = this.listaLibros.filter(data => {
        return data.titulo!== libro.titulo
    })
  }

  eliminar(titulo: string){
    this.listaLibros = this.listaLibros.filter(data => {
      return data.titulo !== titulo;
    });

  }

  eliminarC(titulo:string){
    // console.log('intento eliminar');
    // console.log(titulo, 'elimiar');
  
      for(let i = 0; i < this.listaLibros.length; i ++){
        if(titulo == this.listaLibros[i].titulo){
          // console.log('eli');
          
          this.listaLibros.splice(i, 1);
          localStorage.setItem('Libros', JSON.stringify(this.listaLibros))
        }
      }
      
  
      this.listaLibros = this.listaLibros.filter(data => {
          return data.titulo!== titulo
      })
  }

  modificarlibro(libro: LibroI, libroEli: string){
    this.eliminar(libroEli);
    // console.log(libroEli, 'eliminar esta libro');
    
    this.agregarLibro(libro);
    // console.log(libro, 'Agregar esta libro');
  }
}
