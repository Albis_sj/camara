import { Injectable } from '@angular/core';
import { elementAt } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FiltroService {


  lista_Tipo_1: Filtro_Tipo_1[] = [
    { "id": 1, "name": "Narrativo" },
    { "id": 2, "name": "Dramático" },
    { "id": 3, "name": "Didáctico" },
    { "id": 4, "name": "Educativo" },
  ];

  lista_Tipo_2: Filtro_Tipo_2[] = [
    { "id": 1, "name": "Novela", "tipo1_id": 1 },
    { "id": 2, "name": "Cuento", "tipo1_id": 1  },
    { "id": 3, "name": "Fábula", "tipo1_id": 1 },
    { "id": 4, "name": "Leyenda", "tipo1_id": 1 },
    { "id": 5, "name": "Poesía", "tipo1_id": 1 },

    { "id": 6, "name": "Tragedia", "tipo1_id": 3 },
    { "id": 7, "name": "Comedia", "tipo1_id": 3 },
    { "id": 8, "name": "Drama", "tipo1_id": 3 },
    { "id": 9, "name": "Romance", "tipo1_id": 3 },
    { "id": 10, "name": "Ficción", "tipo1_id": 3 },
    { "id": 11, "name": "Fantasía", "tipo1_id": 3 },
    { "id": 12, "name": "Aventura", "tipo1_id": 3 },

    { "id": 13, "name": "Universitario", "tipo1_id": 4 },
    { "id": 13, "name": "Educativo", "tipo1_id": 4 },
    { "id": 14, "name": "Educativo primaria", "tipo1_id": 4 },
    { "id": 15, "name": "Educativo secundaria", "tipo1_id": 4 },
    { "id": 16, "name": "Educativo Pre-escolar", "tipo1_id": 4 },
    { "id": 17, "name": "Otros", "tipo1_id": 4 },
    { "id": 18, "name": "Autoayuda", "tipo1_id": 4 },
    { "id": 19, "name": "Religioso", "tipo1_id": 4 },

    { "id": 1, "name": "Tecnología", "tipo1_id": 1 },
    { "id": 2, "name": "Sociología", "tipo1_id": 1 },
    { "id": 3, "name": "Comunicación", "tipo1_id": 1 },
    { "id": 4, "name": "Economía", "tipo1_id": 1 },
    { "id": 5, "name": "Derecho", "tipo1_id": 1 },
    { "id": 6, "name": "Psicología", "tipo1_id": 1 },
    { "id": 7, "name": "Metodología", "tipo1_id": 1 },
    { "id": 8, "name": "Medicina", "tipo1_id": 1 },
    { "id": 9, "name": "Farmacología", "tipo1_id": 1 },
    { "id": 10, "name": "Linguística", "tipo1_id": 1 },
    { "id": 11, "name": "Antropología", "tipo1_id": 1 },
    { "id": 12, "name": "Filosofía", "tipo1_id": 1 },
    { "id": 13, "name": "Química", "tipo1_id": 4 },
    { "id": 14, "name": "Física", "tipo1_id": 4 },
    { "id": 15, "name": "Historia", "tipo1_id": 5 },
    { "id": 16, "name": "Ecología", "tipo1_id": 5 },

  ];

  categorias!: categoriaI[];

  lista_Tipo_3: Filtro_Tipo_3[] = [
      { "id": 17, "name": "Gobierno de Bolivia", "tipo2_id": 5 },
  ]

  constructor() { }

  agregarNewCategoria(categoria: categoriaI){
    console.log(categoria);

    if(localStorage.getItem('Categorias') === null){
      this.categorias = [];
      this.categorias.unshift(categoria);
      localStorage.setItem('Categorias', JSON.stringify(this.categorias))
    } else {
      this.categorias = JSON.parse(localStorage.getItem('Categorias') || '');
      this.categorias.unshift(categoria);
      localStorage.setItem('Categorias', JSON.stringify(this.categorias))
    } 
    // console.log(this.listaRegistro, 'lista de agregarTablas');
  }

  ObtenerLocalStorage(){

    if(localStorage.getItem('Categorias') === null){
      console.log('no comments');
      this.categorias = [{
        categoria: '',
      }]
      return false
    } else {
      console.log('podría dar error');

      let categoriaLista = JSON.parse(localStorage.getItem('Categorias') || '')
      console.log(categoriaLista);

      this.categorias = categoriaLista;
      return categoriaLista
      
      
    }
  }

  eliminarCategoria(categoria: categoriaI){

    const response = confirm('Estás seguro?')

    if(response){
      for(let i = 0; i < this.categorias.length; i ++){
        if(categoria == this.categorias[i]){
          // console.log('eli');
          
          this.categorias.splice(i, 1);
          localStorage.setItem('Categorias', JSON.stringify(this.categorias))
        }
      }
      
    }

    
    this.categorias = this.categorias.filter(data => {
      return data.categoria!== categoria.categoria
  })

    // let categoriaLista = JSON.parse(localStorage.getItem('Categorias') || '')
    // console.log(categoriaLista);
    // this.categorias = categoriaLista
    
    // categoriaLista = this.categorias.filter( data => {
    //   return data.categoria != categoria
    // },
    // )
    // console.log(categoriaLista);
    
    // this.categorias = this.categorias.filter(data => {
    //   return data.categoria !== categoria
    // })
  }

  buscarCategoria(id: string): categoriaI{
    return this.categorias.find(e => e.categoria=== id) || {} as categoriaI
  }


}

export interface categoriaI {
  categoria: string
}


export interface Filtro_Tipo_1 {
  id: number,
  name: string
}

export interface Filtro_Tipo_2 {
  id: number,
  name: string,
  tipo1_id: number
}

export interface Filtro_Tipo_3{
  id: number,
  name: string,
  tipo2_id: number
}