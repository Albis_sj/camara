export interface LibroI {
  idioma: string,
  titulo: string,
  subtitulo: string,
  autor_name: string,
  autor_last: string,
  descripcion: string,
  precio: number,
  editorial: string,
  fecha: number,
  formato: string,
  categoria: string
}
