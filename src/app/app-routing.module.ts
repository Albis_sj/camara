import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AsociadosComponent } from './component/asociados/asociados.component';
import { ContactoComponent } from './component/contacto/contacto.component';
import { InicioComponent } from './component/inicio/inicio.component';
import { NosotrosComponent } from './component/nosotros/nosotros.component';
import { PrivadoComponent } from './component/privado/privado.component';
import { TiendaComponent } from './component/tienda/tienda.component';

const routes: Routes = [
  {path: 'nosotros', component: NosotrosComponent},
  {path: 'index', component: InicioComponent},
  {path: 'contacto', component: ContactoComponent},
  {path: 'asociados', component: AsociadosComponent},
  {path: 'tienda', component: TiendaComponent},
  // {path: 'privado', component: PrivadoComponent},
  { path: 'privado',
  loadChildren: () => import('../../src/app/component/privado/privado.module').then(x => x.PrivadoModule)
},
  {path: '**', pathMatch: 'full', redirectTo: 'index'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
