import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { InicioComponent } from './component/inicio/inicio.component';
import { NavbarComponent } from './component/navbar/navbar.component';
import { FooterComponent } from './component/footer/footer.component';
import { NosotrosComponent } from './component/nosotros/nosotros.component';
import { SharedModule } from './component/shared/shared.module';
import { ContactoComponent } from './component/contacto/contacto.component';
import { AsociadosComponent } from './component/asociados/asociados.component';
import { TiendaComponent } from './component/tienda/tienda.component';


@NgModule({
  declarations: [
    AppComponent,
    InicioComponent,
    NavbarComponent,
    FooterComponent,
    NosotrosComponent,
    ContactoComponent,
    AsociadosComponent,
    TiendaComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    SharedModule,

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
