import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  formulario!: FormGroup;
  mensaje!: string;
  enviado: string = 'El mensaje fue enviado con éxito';

  constructor(private fb: FormBuilder) { 
    this.crearFormulario();
  }

  crearFormulario (): void{
    this.formulario = this.fb.group({
      nombre: ['', [Validators.required, Validators.minLength(3)]],
      email: ['', [Validators.required, Validators.pattern(/^(([^<>()\[\]\.,;:\s@\”]+(\.[^<>()\[\]\.,;:\s@\”]+)*)|(\”.+\”))@(([^<>()[\]\.,;:\s@\”]+\.)+[^<>()[\]\.,;:\s@\”]{2,})$/)]],
      mensaje: ['', [Validators.required, Validators.minLength(7)]],
    })
  }

  //GET
  get nombreNoValido(){
    return this.formulario.get('nombre')?.invalid && this.formulario.get('nombre')?.touched;
  }
  
  get emailNoValido(){
    return this.formulario.get('email')?.invalid && this.formulario.get('email')?.touched
  }

  get mensajeNoValido(){
    return this.formulario.get('mensaje')?.invalid && this.formulario.get('mensaje')?.touched
  }

  ngOnInit(): void {
  }

  guardar():void{

    if(this.formulario.valid){

      this.mensaje = this.enviado;
      this.limpiarFormulario();
  
      setTimeout(() => {
        this.mensaje = '';
      }, 3000);

    } else {
      this.mensaje = "El formulario no es válido";
  
      setTimeout(() => {
        this.mensaje = '';
      }, 5000);
    }

  }

  limpiarFormulario(){
    this.formulario.reset()
  }

}
