import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PrivadoRoutingModule } from './privado-routing.module';
import { PrivadoComponent } from './privado.component';
import { LibrosComponent } from './libros/libros.component';
import { SharedModule } from '../shared/shared.module';
import { EditorialComponent } from './editorial/editorial.component';
import { CategoriasComponent } from './categorias/categorias.component';
import { EditarCategoriaComponent } from './categorias/editar-categoria/editar-categoria.component';


@NgModule({
  declarations: [
    PrivadoComponent,
    LibrosComponent,
    EditorialComponent,
    CategoriasComponent,
    EditarCategoriaComponent
  ],
  imports: [
    CommonModule,
    PrivadoRoutingModule,
    SharedModule
  ]
})
export class PrivadoModule { }
