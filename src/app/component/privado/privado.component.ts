import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-privado',
  templateUrl: './privado.component.html',
  styleUrls: ['./privado.component.css']
})
export class PrivadoComponent implements OnInit {

  constructor(private router: ActivatedRoute) { 
    this.router.params.subscribe( parametros => {
      console.log('ruta padre');
      console.log(parametros);
      
    })
  }

  ngOnInit(): void {
  }

}
