import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LibroI } from 'src/app/interfaces/libro.interface';
import { EditorialService, I_editorial } from 'src/app/services/editorial.service';
import { categoriaI, FiltroService, Filtro_Tipo_1, Filtro_Tipo_2, Filtro_Tipo_3 } from 'src/app/services/filtro.service';
import { LibroService } from 'src/app/services/libro.service';

@Component({
  selector: 'app-libros',
  templateUrl: './libros.component.html',
  styleUrls: ['./libros.component.css']
})
export class LibrosComponent implements OnInit {

  enviado: string = 'Se añadió el libro';
  mensaje!: string;
  mostrarMensaje: boolean= false;

  lista_libros!: LibroI[];

  idioma: string[] = [ 'Español', 'Inglés', 'Quechua', 'Otro' ]

  categorias!: categoriaI[];

  tipo_1!: Filtro_Tipo_1[];
  tipo_2!: Filtro_Tipo_2[];
  tipo_3!: Filtro_Tipo_3[];

  editoriales!: I_editorial[];

  formulario!: FormGroup;


  
  propiedades: any = {
    error: false
  }

  constructor(private Svc_filtro: FiltroService,
              private Svc_editorial: EditorialService,
              private Svc_libro: LibroService,
              private fb: FormBuilder) { 
                this.crearFormulario();
                this.mostrarLibros()
              }

  crearFormulario(): void{
    console.log('crcear Form');
    console.log(`idioma\ntitulo\nsubtitulo\nserie_name\nserie_num\nedicion_num\nautor_name\nautor_last\ndescripcion\nprecio\neditorial\nfecha`);
    
    this.formulario = this.fb.group({
      idioma: ['', Validators.required],
      titulo: ['', [Validators.required, Validators.minLength(3)]],
      subtitulo: ['', Validators.minLength(3)],
      autor_name: ['', [Validators.required, Validators.minLength(3)]],
      autor_last: ['', Validators.minLength(3)],
      descripcion: ['', [Validators.required, Validators.minLength(5)]],
      precio: ['', Validators.required],
      editorial: ['', ],
      fecha: ['', Validators.required],
      formato: ['',],
      categoria: ['',]

    })
  }

  ngOnInit(): void {

    this.tipo_1 = this.Svc_filtro.lista_Tipo_1,
    this.tipo_2 = this.Svc_filtro.lista_Tipo_2,
    this.tipo_3 = this.Svc_filtro.lista_Tipo_3,

    this.editoriales = this.Svc_editorial.ObtenerLocalStorage();
    console.log(this.editoriales);
    
    this.categorias = this.Svc_filtro.ObtenerLocalStorage();
    console.log(this.categorias);    
  }

  guardar(): void{
    console.log('guardado');
    console.log(this.formulario.value);

    if(this.formulario.valid){
      this.mensaje = this.enviado;
      setTimeout(() => {
        this.mensaje = '';
      }, 4000);

    this.propiedades.error = true;
    setTimeout(() => {
      this.propiedades.error =false;
    }, 4000);
  }

  this.Svc_libro.agregarLibro(this.formulario.value)
  console.log('click guardar');

  this.limpiarFormulario();
  }
  
  subirInfo(){
    // const libro: LibroI = {}
  }

  limpiarFormulario(){
    this.formulario.reset();
  }

  
  mostrarLibros(){
    this.lista_libros = this.Svc_libro.obtenerLocalStorage();
    console.log(this.lista_libros);
  }

  eliminarLibro(libro: LibroI){
    
    console.log('elimianr categoria TS'); 
    this.Svc_libro.eliminarlibro(libro)
  }

}
