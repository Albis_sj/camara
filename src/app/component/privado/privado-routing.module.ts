import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CategoriasComponent } from './categorias/categorias.component';
import { EditorialComponent } from './editorial/editorial.component';
import { LibrosComponent } from './libros/libros.component';
import { PrivadoComponent } from './privado.component';

const routes: Routes = [
  {path: '', component: PrivadoComponent,
    children:
  [
    {path: 'libros', component: LibrosComponent},
    {path: 'editorial', component: EditorialComponent},
    {path: 'categoria', component: CategoriasComponent},
    {path: '**', pathMatch: 'full', redirectTo: 'libros'}
  ]},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PrivadoRoutingModule { }

