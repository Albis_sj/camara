import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { FiltroService } from 'src/app/services/filtro.service';

@Component({
  selector: 'app-editar-categoria',
  templateUrl: './editar-categoria.component.html',
  styleUrls: ['./editar-categoria.component.css']
})
export class EditarCategoriaComponent implements OnInit {

  formulario!: FormGroup;
  
  constructor(private activatedRoute: ActivatedRoute,
              private router: Router,
              private fb: FormBuilder,
              private _categoriaSVC: FiltroService) {
    this.activatedRoute.params.subscribe(params => {
      const id = params['id'];

      this.formulario = this.fb.group({
        categorias: ['', [Validators.minLength(3)]]
      });

      if(id !== 'nuevo'){
        const categoria = this._categoriaSVC.buscarCategoria(id);

        if(Object.keys(categoria).length === 0){
        }

      }
    })
  }

  ngOnInit(): void {
  }

}
