import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { categoriaI, FiltroService } from 'src/app/services/filtro.service';

@Component({
  selector: 'app-categorias',
  templateUrl: './categorias.component.html',
  styleUrls: ['./categorias.component.css']
})
export class CategoriasComponent implements OnInit {

  enviado: string = 'Se añadió la categoría';
  mensaje!: string;

  propiedades: any = {
    error: false
  }

  categoriaLista!: categoriaI[];

  formulario!: FormGroup;
  constructor(private fb: FormBuilder,
              private _filtroSVC: FiltroService,
              private router: Router) { 

    this.crearFormulario();

  }

  get categoriaNoValido(){
    return this.formulario.get('categoria')?.invalid && this.formulario.get('categoria')?.touched
  }

  crearFormulario(): void{
    console.log('crcear Form');
    // console.log(`idioma\ntitulo\nsubtitulo\nserie_name\nserie_num\nedicion_num\nautor_name\nautor_last\ndescripcion\nprecio\neditorial\nfecha`);
    
    this.formulario = this.fb.group({
      categoria: ['', [Validators.required, Validators.minLength(3)]]
    })
  }

  ngOnInit(): void {

    this.mostrarCategorias()
  }

  guardar(): void{
    console.log('guardado');
    console.log(this.formulario.value);
    
    if(this.formulario.valid){
      this.mensaje = this.enviado;
      setTimeout(() => {
        this.mensaje = '';
      }, 4000);

    this.propiedades.error = true;
    setTimeout(() => {
      this.propiedades.error =false;
    }, 4000);
  }

    this._filtroSVC.agregarNewCategoria(this.formulario.value)

    this.limpiarFormulario()

  }

  limpiarFormulario(){
    this.formulario.reset()
  }


  mostrarCategorias(){
    this.categoriaLista = this._filtroSVC.ObtenerLocalStorage()
    console.log(this.categoriaLista);
  }

  eliminarCategoria(categoria: categoriaI){

    console.log('elimianr categoria TS'); 
    this._filtroSVC.eliminarCategoria(categoria)
  }

  // MODIFICAR(categoria: string){
  //   this.router.navigate(['/editar-categoria/', categoria])
  // }

  // modificarCategoria(categoriaList: categoriaI, categoria: string){
  //   this.eliminar(categoria);

  //   this.agregarTabla(categoriaList)
  // }

  eliminar(categoria: string){
    this.categoriaLista = this.categoriaLista.filter(data => {
      return data.categoria !== categoria
    })
  }

  agregarTabla(categoriaList: categoriaI){
    if(localStorage.getItem('Categoria') === null){
      this.categoriaLista = [];
      this.categoriaLista.unshift(categoriaList);
      localStorage.setItem('Categoria', JSON.stringify(this.categoriaLista))
    } else {
      this.categoriaLista = JSON.parse(localStorage.getItem('Categoria') || '');
      this.categoriaLista.unshift(categoriaList);
      localStorage.setItem('Categoria', JSON.stringify(this.categoriaLista))
    }

  }
}
