import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { EditorialService, I_editorial } from 'src/app/services/editorial.service';

@Component({
  selector: 'app-editorial',
  templateUrl: './editorial.component.html',
  styleUrls: ['./editorial.component.css']
})
export class EditorialComponent implements OnInit {

  enviado: string = 'Se añadió la editorial';
  mensaje!: string;

  formulario!: FormGroup;
  editorialLista!: I_editorial[];

  propiedades: any = {
    error: false
  }

  constructor( private fb: FormBuilder,
              private _editorialSVC: EditorialService) { 

    this.crearFormulario();
  }
  
  get nombreNoValido(){
    return this.formulario.get('nombre')?.invalid && this.formulario.get('nombre')?.touched
  }

  get telefono1NoValido(){
    return this.formulario.get('telefono1')?.invalid && this.formulario.get('telefono1')?.touched
  }
  
  get telefono2NoValido(){
    return this.formulario.get('telefono2')?.invalid
  }

  get celular1NoValido(){
    return this.formulario.get('celular1')?.invalid
  }
  
  get celular2NoValido(){
    return this.formulario.get('celular2')?.invalid
  }
  
  get correo1NoValido(){
    return this.formulario.get('correo1')?.invalid
  }
    
  get correo2NoValido(){
    return this.formulario.get('correo2')?.invalid
  }

  crearFormulario() {
    
    this.formulario = this.fb.group({
      nombre: ['', [Validators.required, Validators.minLength(3)]],
      telefono1: ['', Validators.minLength(7)],
      telefono2: ['', Validators.minLength(7)],
      celular1: ['', Validators.minLength(8)],
      celular2: ['', Validators.minLength(8)],
      direccion: ['', Validators.minLength(5)],
      correo1: ['', Validators.pattern(/^(([^<>()\[\]\.,;:\s@\”]+(\.[^<>()\[\]\.,;:\s@\”]+)*)|(\”.+\”))@(([^<>()[\]\.,;:\s@\”]+\.)+[^<>()[\]\.,;:\s@\”]{2,})$/)],
      correo2: ['', Validators.pattern(/^(([^<>()\[\]\.,;:\s@\”]+(\.[^<>()\[\]\.,;:\s@\”]+)*)|(\”.+\”))@(([^<>()[\]\.,;:\s@\”]+\.)+[^<>()[\]\.,;:\s@\”]{2,})$/)],
      url: ['', Validators.minLength(3)]
    })
  }

    ngOnInit(): void {
      // console.log(`nombre\ntelefono1\ntelefono2\ncelular\ncelular2\ndireccion\ncorreo1\ncorreo2\nurl`);

      this.mostrarEditoriales();
    }

  guardar(){
    console.log('guardado');
    console.log(this.formulario.value);

    if(this.formulario.valid){
      this.mensaje = this.enviado;
      setTimeout(() => {
        this.mensaje = '';
      }, 4000);

    this.propiedades.error = true;
    setTimeout(() => {
      this.propiedades.error =false;
    }, 4000);
  }

  this._editorialSVC.agregarNewEditorial(this.formulario.value)

  this.limpiarFormulario();
  }

  mostrarEditoriales(){
    this.editorialLista = this._editorialSVC.ObtenerLocalStorage()
    console.log(this.editorialLista);
  }
  
  limpiarFormulario(){
    this.formulario.reset();
  }

  eliminarEditorial(nombre: I_editorial){

    console.log('elimianr Editorial TS', nombre);    
    this._editorialSVC.eliminarEditorial(nombre)
  }
}
